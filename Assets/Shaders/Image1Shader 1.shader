﻿Shader "Costum/Unlit/Exercice1/Image1"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_CircleRadius("Circle Radius", Range(0.01,2)) = 1
		_NbCircle("Nb Circle", Int) = 2
		_Color1("Color 1", Color) = (0.25,0,0,1)
		_Color2("Color 2", Color) = (1,0,0,1)
		_ColorLerpCoef("Color Lerp Coef", Range(0.01,1)) = 0

		
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			fixed _CircleRadius;
			fixed _NbCircle;
			fixed4 _Color1;
			fixed4 _Color2;
			float _ColorLerpCoef;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
			
				float2 center = float2(0.5f, 0.5f);
				float distanceToCenter = distance(i.uv, center);
				float circleSize = _ColorLerpCoef / _NbCircle;
				float CircleID = floor(distanceToCenter / circleSize);
				float isInCircle = 1 - smoothstep(_ColorLerpCoef*3 - 0.001f, _ColorLerpCoef*3 + 0.001f, distanceToCenter);
				col = _Color1;
				//circleColor = lerp(_Color1, _Color2, CircleID / (_NbCircle - 1));
				
				col = lerp(col, _Color2, isInCircle);
				
				return col;
			
			}
			ENDCG
		}
	}
}

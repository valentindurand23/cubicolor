﻿using SDD.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : SingletonGameStateObserver<Cube>
{
    COLOR m_Color;
    [SerializeField] Material m_CubeMaterial;
    [SerializeField] Color m_Color1;
    [SerializeField] Color m_Color2;
    [SerializeField] float m_ColorLerpCoef;

    [SerializeField] protected Color[] m_Colors;

    Vector3 m_Direction;
    bool m_IsMoving = false;
    Transform m_Transform;

    Level m_CurrentLevel;

    float m_TimeAcceptInputs;

    public GameObject m_Gfx;

    public override void SubscribeEvents()
    {
        base.SubscribeEvents();
        EventManager.Instance.AddListener<LevelHasBeenInstantiatedEvent>(LevelHasBeenInstantiated);
        EventManager.Instance.AddListener<InColorTileHasBeenReachedEvent>(InColorTileHasBeenReached);
        EventManager.Instance.AddListener<OutColorTileHasBeenReachedEvent>(OutColorTileHasBeenReached);
        EventManager.Instance.AddListener<GoToNextLevelEvent>(GoToNextLevel);
    }

    public override void UnsubscribeEvents()
    {
        base.UnsubscribeEvents();
        EventManager.Instance.RemoveListener<LevelHasBeenInstantiatedEvent>(LevelHasBeenInstantiated);
        EventManager.Instance.RemoveListener<InColorTileHasBeenReachedEvent>(InColorTileHasBeenReached);
        EventManager.Instance.RemoveListener<OutColorTileHasBeenReachedEvent>(OutColorTileHasBeenReached);
        EventManager.Instance.RemoveListener<GoToNextLevelEvent>(GoToNextLevel);
    }

    void SetCubeColor(COLOR color)
    {
        StartCoroutine(ChangeColorCoroutine(m_Color, color, .5f));
    }

    void ResetCubeColor()
    {
        SetCubeColor(COLOR.black);
    }

    void InColorTileHasBeenReached(InColorTileHasBeenReachedEvent e)
    {
        SetCubeColor(e.eColor);
    }
    void OutColorTileHasBeenReached(OutColorTileHasBeenReachedEvent e)
    {
        if (e.eColor == m_Color)
        {
            EventManager.Instance.Raise(new CubeHasCleanedAColorEvent() { eColor = m_Color });
            ResetCubeColor();
        }
    }

    void GoToNextLevel(GoToNextLevelEvent e)
    {
        ResetCubeColor();
    }

    void LevelHasBeenInstantiated(LevelHasBeenInstantiatedEvent e)
    {
        StopAllCoroutines();
        m_CurrentLevel = e.eLevel;
        m_Transform.position = m_CurrentLevel.CubeSpawnPosition;
        m_Transform.rotation = Quaternion.identity;
        m_IsMoving = false;
        ResetCubeColor();
        m_TimeAcceptInputs = Time.time + .5f;
    }

    protected override void Awake()
    {
        base.Awake();
        m_Transform = transform;
    }

    // Update is called once per frame
    void Update()
    {


        if (!GameManager.Instance.IsPlaying || !m_CurrentLevel || Time.time < m_TimeAcceptInputs) return;

        float hInput = Input.GetAxis("Horizontal");
        float vInput = Input.GetAxis("Vertical");

        if (!m_IsMoving)
        {
            Transform camera = GameObject.Find("Main Camera").transform;

            Vector3 dir = Vector3.zero;


            if (Mathf.Abs(hInput) > 0)
            {
                // BUGGED CODE
                Vector3 LookCam = CameraManager.Rotation * Vector3.right * Mathf.Sign(hInput);
                if (Mathf.Abs(LookCam.x) < Mathf.Abs(LookCam.z))
                {
                    dir = Vector3.forward * Mathf.Sign(LookCam.z);// * Mathf.Sign(hInput);
                }
                else if (Mathf.Abs(LookCam.z) < Mathf.Abs(LookCam.x))
                {
                    dir = Vector3.right * Mathf.Sign(LookCam.x);// * Mathf.Sign(hInput);
                }

                

                // END OF BUGGED CODE
            }


            else if (Mathf.Abs(vInput) > 0)
            {
                //StartCoroutine(moveUp(.075f));
                // BUGGED CODE
                Vector3 LookCam = CameraManager.Rotation * Vector3.forward * Mathf.Sign(vInput);
                if (Mathf.Abs(LookCam.x) < Mathf.Abs(LookCam.z))
                {
                    

                    dir = Vector3.forward * Mathf.Sign(LookCam.z);// * Mathf.Sign(hInput);
                }
                else if (Mathf.Abs(LookCam.z) < Mathf.Abs(LookCam.x))
                {
                    

                    dir = Vector3.right * Mathf.Sign(LookCam.x);// * Mathf.Sign(hInput);
                }
                // END OF BUGGED CODE
            }

            if (dir.magnitude != 0)
            {

                Debug.Log(m_CurrentLevel.IsPathWalkable(m_Transform.position + dir));
                dir.Normalize();
                if (m_CurrentLevel.IsPathWalkable(m_Transform.position + dir))
                {
                    RoundRotation90();
                    RoundPosition();
                    EventManager.Instance.Raise(new CubeHasLeftPositionEvent() { ePos = m_Transform.position });
                    StartCoroutine(Rotation90Coroutine(dir, .5f));
                }
                else
                {
                    StartCoroutine(QuickAndShortNoGoRotationCoroutine(dir, .5f, 45f));
                }
            }
        }

        //Cube Material Update
        m_CubeMaterial.SetColor("_Color1", m_Color1);
        m_CubeMaterial.SetColor("_Color2", m_Color2);
        m_CubeMaterial.SetFloat("_ColorLerpCoef", m_ColorLerpCoef);
        m_CubeMaterial.SetVector("_CubeWorldPosition", m_Transform.position);
    }

    Vector3 GetRotAxis(Vector3 direction)
    {
        return Vector3.Cross(Vector3.up, direction);
    }

    Vector3 GetRotCenter(Vector3 direction)
    {
        return m_Transform.position + (direction - Vector3.up) * .5f;
    }

    void RoundRotation90()
    {
        m_Transform.rotation = Quaternion.Euler(Mathf.RoundToInt(m_Transform.rotation.eulerAngles.x / 90f) * 90f,
            Mathf.RoundToInt(m_Transform.rotation.eulerAngles.y / 90f) * 90f,
            Mathf.RoundToInt(transform.rotation.eulerAngles.z / 90f) * 90f);
    }

    void RoundPosition()
    {
        m_Transform.position = new Vector3(Mathf.RoundToInt(m_Transform.position.x), Mathf.RoundToInt(m_Transform.position.y), Mathf.RoundToInt(m_Transform.position.z));
    }

    public IEnumerator QuickAndShortNoGoRotationCoroutine(Vector3 direction, float duration, float angle)
    {
        Debug.Log("QuickAndShortNoGoRotationCoroutine");
        m_IsMoving = true;
        direction.Normalize(); // on ne sait jamais
        Vector3 rotAxis = GetRotAxis(direction);
        Vector3 rotCenter = GetRotCenter(direction);

        yield return StartCoroutine(RotationAroundAxisCoroutine(rotAxis, rotCenter, duration / 2f, angle));
        yield return StartCoroutine(RotationAroundAxisCoroutine(rotAxis, rotCenter, duration / 2f, -angle));
        m_IsMoving = false;
    }

    public IEnumerator RotationAroundAxisCoroutine(Vector3 rotAxis, Vector3 rotCenter, float duration, float angle)
    {
        // BUGGED CODE
        transform.rotation = new Quaternion(0, 0, 0, 0);

        Vector3 target = m_Transform.position;
        m_Transform.position += Vector3.ProjectOnPlane(rotCenter - m_Transform.position, Vector3.up).normalized;
        Quaternion targetQ = m_Transform.rotation;
        targetQ *= Quaternion.AngleAxis(angle, rotAxis);
        m_Gfx.transform.position = target;

        float timer = 0;
        while (timer <= duration)
        {
            timer += Time.deltaTime;

            m_Gfx.transform.position = Vector3.Lerp(m_Gfx.transform.position, transform.position, timer / duration);

            transform.rotation = Quaternion.Slerp(m_Gfx.transform.rotation, targetQ, timer / duration);

            yield return duration;
        }

        //m_Transform.position += Vector3.ProjectOnPlane(rotCenter - m_Transform.position, Vector3.up).normalized;
        yield return new WaitForSeconds(duration);

        //m_Transform.position += Vector3.ProjectOnPlane(rotCenter - m_Transform.position, Vector3.up).normalized;
        //yield return duration;

        // END OF BUGGED CODE
    }

    public IEnumerator Rotation90Coroutine(Vector3 direction, float duration)
    {


        m_IsMoving = true;
        direction.Normalize(); // on ne sait jamais
        Vector3 rotAxis = GetRotAxis(direction);
        Vector3 rotCenter = GetRotCenter(direction);
        yield return StartCoroutine(RotationAroundAxisCoroutine(rotAxis, rotCenter, duration, 90f));
        EventManager.Instance.Raise(new CubeHasReachedPositionEvent() { ePos = m_Transform.position });
        m_IsMoving = false;



    }

    public IEnumerator ChangeColorCoroutine(COLOR startColor, COLOR endColor, float duration)
    {
        m_Color = endColor;

        m_Color1 = m_Colors[(int)startColor];
        m_Color2 = m_Colors[(int)endColor];
        m_ColorLerpCoef = 0;

        yield return new WaitForEndOfFrame();

        float elapsedTime = 0;
        while (elapsedTime < duration)
        {
            float k = elapsedTime / duration;
            k = Mathf.Sin(k * Mathf.PI / 2f);
            m_ColorLerpCoef = k;
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        m_ColorLerpCoef = 1f;
    }


}

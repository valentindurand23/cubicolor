﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    Vector3 Target;
    Vector3 Target2;
    Quaternion TargetQ;
    Quaternion Target2Q;
    public float startTime;
    public float speed;
    public float duration = 0.5f;
    public float timer = 0;
    bool isUp = false;
    // Start is called before the first frame update
    void Start()
    {
        float rayon = (Mathf.Sqrt(2) / 2) - 0.5f ;
        
        Target = new Vector3(transform.position.x + 0.5f, transform.position.y + rayon, transform.position.z);
        Target2 = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);

        TargetQ = transform.rotation;
        TargetQ *= Quaternion.AngleAxis(45, -Vector3.forward);

    }

    // Update is called once per frame
    void Update()
    {
        //Target = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);
        timer += Time.deltaTime;

        if (transform.position == Target && isUp == false)
        {
            timer = 0;
            isUp = true;
            Target2Q = transform.rotation;

            Target2Q *= Quaternion.AngleAxis(45, -Vector3.forward);

        }

        //transform.rotation = Quaternion.Slerp(transform.rotation, TargetQ, timer / duration);
        //transform.position = Vector3.Slerp(transform.position, Target, timer / duration);


        if (isUp == true)
        {
            transform.position = Vector3.Slerp(transform.position, Target2, timer / duration);
            transform.rotation = Quaternion.Lerp(transform.rotation, Target2Q, timer / duration);
        }
        else
        {
            transform.position = Vector3.Slerp(transform.position, Target, timer / duration);

            transform.rotation = Quaternion.Lerp(transform.rotation, TargetQ, timer / duration);


        }

        //transform.position = Vector3.Slerp(transform.position, Target2, 0.01f);





    }


}

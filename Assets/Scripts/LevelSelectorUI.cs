﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using SDD.Events;

public class LevelSelectorUI :MonoBehaviour
{
	[SerializeField] ToggleGroup m_LevelSelectorToggleGroup;
	[SerializeField] string m_LevelToggleFormatString;
    [SerializeField] GameObject LevelTemplate;


    private void Awake()
    {
        GameObject levelHud = GameObject.Find("LevelSelectionGrid");
        int nb_level = LevelsManager.Instance.m_nb;

        for (int i = 1; i < nb_level + 1; i++)
        {
            GameObject levelInCreate = Instantiate(LevelTemplate);
            levelInCreate.name = i.ToString();
            levelInCreate.GetComponentInChildren<Text>().text = "Level " + i.ToString();
            levelInCreate.transform.parent = levelHud.transform;
            levelInCreate.GetComponent<Toggle>().group = m_LevelSelectorToggleGroup;
            levelInCreate.GetComponent<Toggle>().onValueChanged.AddListener(SelectedLevelHasChanged);


        }
    }

    void SetLevelParameters()
	{
		SetCurrentLevel(LevelsManager.Instance.CurrentLevelIndex);
		//SetMaxLevel(LevelsManager.Instance.BestLevelIndex);
		SetMaxLevel(15);
	}

	void SetCurrentLevel(int index)
	{
		m_LevelSelectorToggleGroup.SetAllTogglesOff();
		string toggleName = string.Format(m_LevelToggleFormatString, index+1);
		GetComponentsInChildren<Toggle>().Where(item => item.name.Equals(toggleName)).FirstOrDefault().isOn = true; 
	}

	void SetMaxLevel(int maxLevelIndex)
	{
		GetComponentsInChildren<Toggle>().ToList().ForEach(item => item.interactable = (int.Parse(item.name) - 1 <= maxLevelIndex));
	}

	IEnumerator Start()
	{
		while (!LevelsManager.Instance) yield return null;
		SetLevelParameters();
	}

	private void OnEnable()
	{
		if (!LevelsManager.Instance) return;
		SetLevelParameters();
	}

	public void SelectedLevelHasChanged(bool value)
	{
		if(value)
		{
			foreach(Toggle activeToggle in m_LevelSelectorToggleGroup.ActiveToggles())
				EventManager.Instance.Raise(new SelectedLevelIndexHasChangedEvent() {eSelectedLevelIndex = int.Parse(activeToggle.name)-1 });
		}
	}
}

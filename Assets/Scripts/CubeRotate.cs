﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeRotate : MonoBehaviour
{

    public GameObject gfx;

    public GameObject center;

    public GameObject up;
    public GameObject down;
    public GameObject right;
    public GameObject left;

    public int step = 9;
    public float speed = (float)0.01;
    public float duration = 0.5f;

    bool input = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (input == true)
        {
            if (Input.GetKey(KeyCode.UpArrow))

            {
                Debug.Log("up");

                StartCoroutine("moveUp");
                input = false;
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                StartCoroutine("moveDown");
                input = false;
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                StartCoroutine("moveRight");
                input = false;
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                StartCoroutine("moveLeft");
                input = false;
            }


        }
    }

    IEnumerator moveUp()
    {
        transform.rotation = new Quaternion(0, 0, 0, 0);

        Quaternion TargetQ = transform.rotation;
        TargetQ *= Quaternion.AngleAxis(90, Vector3.right);
        float timer = 0;

        while ( timer <= duration)
        {

            transform.rotation = Quaternion.Lerp(transform.rotation, TargetQ,timer / duration);

            timer += Time.deltaTime;


            yield return new WaitForSeconds(speed);
        }
        input = true;
    }
    IEnumerator moveDown()
    {
        transform.rotation = new Quaternion(0, 0, 0, 0);

        Quaternion TargetQ = transform.rotation;
        TargetQ *= Quaternion.AngleAxis(90, -Vector3.right);
        float timer = 0;

        while (timer <= duration)
        {

            transform.rotation = Quaternion.Lerp(transform.rotation, TargetQ, timer / duration);

            timer += Time.deltaTime;


            yield return new WaitForSeconds(speed);
        }
        input = true;
    }
    IEnumerator moveRight()
    {
        transform.rotation = new Quaternion(0, 0, 0, 0);

        Quaternion TargetQ = transform.rotation;
        TargetQ *= Quaternion.AngleAxis(90, Vector3.forward);
        float timer = 0;

        while (timer <= duration)
        {

            transform.rotation = Quaternion.Lerp(transform.rotation, TargetQ, timer / duration);

            timer += Time.deltaTime;


            yield return new WaitForSeconds(speed);
        }
        input = true;
    }
    IEnumerator moveLeft()
    {
        transform.rotation = new Quaternion(0, 0, 0, 0);

        Quaternion TargetQ = transform.rotation;
        TargetQ *= Quaternion.AngleAxis(90, -Vector3.forward);
        float timer = 0;

        while (timer <= duration)
        {

            transform.rotation = Quaternion.Lerp(transform.rotation, TargetQ, timer / duration);

            timer += Time.deltaTime;


            yield return new WaitForSeconds(speed);
        }
        input = true;
    }

}
